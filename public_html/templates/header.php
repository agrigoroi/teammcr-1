<html>
<head>
<title>Library @ #DevXS : Alpha Land</title>
<link REL="SHORTCUT ICON" HREF="img/favicon.ico">
<link rel="stylesheet" type="text/css" href="templates/style.css" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
</head>

<body>

<nav id="primarymenu">
	<ul>
		<li><a href="/">Home</a></li>
		<li><a href="search.php">Module Search</a></li>
		<li><a href="libraryapp.pdf">Wireframes</a></li>
	</ul>
</nav>
<header>
<div class="wrapper">
    <div id="uni">
	<img src="img/uni.png" alt="University Logo" />
	<span id="uni"><?=$req_user_info['universityname']?></span>
	<a href="./logout.php">Click here to logout</a>
    </div>
    <h1><img src="img/library.png" alt="Library Icon" /> #DevXS Library App</h1>
    <h2>Alpha Land</h2>

</div>
</header>
<div class="wrapper">

