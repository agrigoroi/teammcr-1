</div><!--end wrapper-->
<footer>
    Created by Team MCR!
</footer>
<script>
    $("#bookinfo").show();
    $("#review").hide();
    $("#location").hide();
    $("#similar").hide();

    $("#reviewlink").click(function ( event ) {
        event.preventDefault();
        $("#bookinfo").hide();
        $("#review").show();
        $("#location").hide();
        $("#similar").hide();
    });
    $("#bookinfolink").click(function ( event ) {
        event.preventDefault();
        $("#bookinfo").show();
        $("#review").hide();
        $("#location").hide();
        $("#similar").hide();
    });
    $("#locationlink").click(function ( event ) {
        event.preventDefault();
        $("#bookinfo").hide();
        $("#review").hide();
        $("#location").show();
        $("#similar").hide();
    });
    $("#similarlink").click(function ( event ) {
        event.preventDefault();
        $("#bookinfo").hide();
        $("#review").hide();
        $("#location").hide();
        $("#similar").show();
    });

</script>

</body>
</html>
