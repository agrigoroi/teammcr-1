<?php

function getScribdBooks($bookname, $resultlimit = 10)
{
  $url = 'http://api.scribd.com/api?method=docs.search&api_key=5i47gv98jfx60qca81k6t&query='.$bookname.'&scope=all';
  $str = file_get_contents($url);
  $xml = simplexml_load_file($url);
  
  for($resultid = 0; $resultid < $resultlimit; $resultid++)
  {
    $title = $xml->result_set[0]->result[$resultid]->title;
    $description = $xml->result_set[0]->result[$resultid]->description;
    $pages = $xml->result_set[0]->result[$resultid]->page_count;
    $docid = $xml->result_set[0]->result[$resultid]->doc_id;
    $link = 'http://www.scribd.com/doc/'.$docid;
	
    $arr[$resultid] = array( 'title'       => $title,
                             'pages'       => $pages,
                             'description' => $description,
                             'link'        => $link );
  };
  return $arr;
};?>
