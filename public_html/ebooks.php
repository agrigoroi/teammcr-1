<?php
include("./lib/config.php");
include("./lib/database.php");

function getScribdBooks($bookname, $resultlimit = 10)
{
  $url = 'http://api.scribd.com/api?method=docs.search&api_key=28ltwbexsyxm5hx3lbnh2&query='.urlencode($bookname).'&scope=all';
  $xml = simplexml_load_file($url);
  
  for($resultid = 0; $resultid < $resultlimit; $resultid++)
  {
    $title = $xml->result_set[0]->result[$resultid]->title;
    $description = $xml->result_set[0]->result[$resultid]->description;
    $pages = $xml->result_set[0]->result[$resultid]->page_count;
    $docid = $xml->result_set[0]->result[$resultid]->doc_id;
    $link = 'http://www.scribd.com/doc/'.$docid;
	
    $arr[$resultid] = array( 'title'       => $title,
                             'pages'       => $pages,
                             'description' => $description,
                             'link'        => $link );
  };
  return $arr;
};

$arr = getScribdBooks("Fall");
//echo $arr[0]['title'];?>
<?php 
for($i = 0; $i < 10; $i++)
{
echo
'<html>
<body>
<a href="'.
$arr[$i]['link'].' ">'.$arr[$i]['title'].'</a>
<br>
</body>
</html>';

}

?>