<?php
include("./lib/config.php");
include("./lib/database.php");
include("./lib/session.php");

if($session->logged_in) {
  header("Location: ./index.php");
}
if(isset($_POST['login'])) {
  if(trim($_POST['username']) == ""){
    $loginerror = "You have not entered a username.";
  }else if(trim($_POST['password']) == ""){
    $loginerror = "You have not entered a password";
  }else if(trim($_POST['university']) == ""){
    $loginerror = "You have not selected a university";
  }else if(!mysql_num_rows(mysql_query("SELECT * FROM `students` WHERE username = '".mysql_real_escape_string($_POST['username'])."' AND universityid = '".mysql_real_escape_string($_POST['university'])."'"))) {
    $loginerror = "You have not entered a valid username.";
  }else if(!mysql_num_rows(mysql_query("SELECT * FROM `students` WHERE username = '".mysql_real_escape_string($_POST['username'])."' AND password = '".md5($_POST['password'])."' AND universityid = '".mysql_real_escape_string($_POST['university'])."'"))) {
    $loginerror = "You have entered an invalid password.";
  }else{
    $session->userinfo = $database->getUserInfo($_POST['username']);
    $session->username = $_SESSION['username'] = $session->userinfo['username'];
    $session->userid   = $_SESSION['userid']   = $session->generateRandID();
    
    mysql_query("UPDATE students SET userid = '".$session->userid."', lastactive = '" . time() . "' WHERE username = '".$session->username."'");
    
    $cookie_expire = time() + (60*60*24*100);
    setcookie("user", $session->username, $cookie_expire, "/");
    setcookie("uid", $session->userid, $cookie_expire, "/");
    
    header("Location: ./index.php");
  }
}

include("./templates/header.php");
?>
<form action="login.php" method="post">
	<h2>Welcome to Library@DevXS</h2>
	<h4>Please login below</h4>
<?php
if(isset($loginerror)) {
  print("	<div style=\"color: red; padding: 5px; margin-top:-25px;\"><b>Error:</b> " . $loginerror . "</div>\n");
}
?>
	<fieldset>
		<label for="university">University:</label>
		<select name="university" id="university">
<?php
$result2 = mysql_query("SELECT * FROM `universities` ORDER BY `ucas_code` ASC");
while($row2 = mysql_fetch_array($result2)) {
  print("			<option value=\"".trim($row2['ucas_code'])."\">".trim($row2['university_name'])."</option>\n");
}
?>
		</select>
	</fieldset>
	<fieldset>
		<label for="username">Username:</label>
		<input type="text" name="username" value="<?php print htmlspecialchars($_POST['username']); ?>" id="username">
	</fieldset>
	<fieldset>
		<label for="password">Password:</label>
		<input type="password" name="password" id="password">
	</fieldset>
	<fieldset>
		<input type="submit" name="login" value="Login" id="login">
	</fieldset>
</form>
<?php
include("./templates/footer.php");
?>
