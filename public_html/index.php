<?php
include("./lib/config.php");
include("./lib/database.php");
include("./lib/session.php");

if(!$session->logged_in) {
  header("Location: ./login.php");
}

$req_user = $session->username;
$req_user_info = $database->getUserInfo($req_user);

function getUniversity($university_id) {
	$result = mysql_query("SELECT * FROM `universities` WHERE `ucas_code` = '" . $university_id . "'");
	$row = mysql_fetch_row($result);
	return $row[1];
}

$list = mysql_query("SELECT * FROM courses ORDER BY courseid ASC");

while($cat = mysql_fetch_array($list)) {
	$booklist .= '<option value="'.$cat['courseid'].'">'.$cat['coursename'].'</option>'.PHP_EOL;
}

include("./templates/header.php");
?>
<div id="top">
	<h2>Welcome to Library@DevXS</h2>
	<p>Hello <?php print($req_user_info['name']); ?></p>
	<p>It is Semester 1.</p>
	<p>You are studying at : <?=$req_user_info['module']?></p>
</div>

<div id="content"> 
<form action="search.php" method="post">
    <div class="split left">
        <h2>Search Your Module Reading List</h2>
	<select name="type[]" multiple size="10"><?=$booklist?></select>
    </div>
	
        <div class="split right">
            <h2>Advanced Search</h2>
		<fieldset>
			<label for="Bookname">Book Name</label>
			<input type="text" name="bookname" id="Bookname">
		</fieldset>
		<fieldset>
			<label for="free">Free Books</label>
			<input type="checkbox" checked="checked" name="booksearch" id="free">
		</fieldset>
		<fieldset>
			<label for="ebooks">E-Books</label>
			<input type="checkbox" name="e-booksearch" id="ebooks">
		</fieldset>
		<fieldset>
			<label for="physical">Physical Books</label>
			<input type="checkbox" name="physicalbooksearch" id="physical">
		</fieldset>
		<fieldset>
			<label for="student">Student Recommendations</label>
			<input type="checkbox" name="studentrecommendationsearch" id="student">
		</fieldset>
		<fieldset>
			<label for="lecturer">Lecturer Recommendations</label>
			<input type="checkbox" name="lecturerecommendationsearch" id="lecturer">
		</fieldset>
        </div>
    <hr />
    <input type="submit" name="search" value="Search" />
	
</form>
</div>
<?php
include("./templates/footer.php");
?>