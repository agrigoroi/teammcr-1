<?php
include("./lib/config.php");
include("./lib/database.php");
include("./lib/session.php");

if($session->logged_in) {
  if(isset($_COOKIE['user']) && isset($_COOKIE['uid'])) {
    $cookie_expire = time() - (60*60*24*100);
    setcookie("user", "", $cookie_expire, "/");
    setcookie("uid", "", $cookie_expire, "/");
  }
  
  unset($_SESSION['username']);
  unset($_SESSION['userid']);
  
  $session->logged_in = false;
  $session->username  = "Guest";
  
  header("Location: ./login.php");
}else{
  header("Location: ./login.php");
}
?>