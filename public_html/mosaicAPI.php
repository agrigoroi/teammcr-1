<?php

// Search the mosaic Database for the most similar books to the given book;
// Get the coursecode student from which loaned this book the most;
// Then looks for the most loaned book by student from that course;
// Because of a bug, sometimes two books may be equal;
// Apparently this is because of some strange behavior of the in_array function
// when the array contains strings of digits.
// The number of elements outputed will not be exactly equal to resultlimit,
// but will be almost equal to it (+-1)
// bookid is also retured from the function
// bookid: Book ISBN code;
// resultlimit: The number of result to return
 
function getSimilarBooks($bookid, $resultlimit=8)
{
  $numberOfLoops = 1;
  if($resultlimit>5)
  {
    $resultlimit /= 2;
    $numberOfLoops = 2;
  }
  $url = 'http://library.hud.ac.uk/mosaic/api.pl?isbn='.urlencode($bookid).'&show=summary';
  $xml = simplexml_load_file($url);
  $bestmatch = array();
  for($count = 0; $count < $resultlimit; $count++)
  {
    $maxLoan = 0;
    foreach($xml->summary[0]->loansPerCourseCode as $loanedTimes)
    {
      $number = intval($loanedTimes);
      if(($number > $maxLoan) && (!in_array($loanedTimes['courseCode'], $bestmatch) ) )
      {
        $maxLoan = $number;
        $bestmatch[$count] = $loanedTimes['courseCode'];
      }
    }
  }
  $bestBooks = array();
  array_push($bestBooks, $bookid);
  foreach($bestmatch as $bestcourse)
  {
    $url = 'http://library.hud.ac.uk/mosaic/api.pl?ucas='.urlencode($bestcourse).'&show=summary';
    $xml = simplexml_load_file($url);
    for($justAPointlessVariable = 0; $justAPointlessVariable < $numberOfLoops; $justAPointlessVariable++)
    {
      $bestBook = NULL;
      $maxLoan = 0;
      foreach($xml->summary[0]->loansPerGlobalID as $loanedTimes)
      {
        $number = intval($loanedTimes);
        if(($number > $maxLoan) && (!in_array($loanedTimes['globalID'], $bestBooks)))
        {
          $maxLoan = $number;
          $bestBook = $loanedTimes['globalID'];
        }
      }
      if(!in_array($loanedTimes['globalID'], $bestBooks))
        array_push($bestBooks, $bestBook);
    }
  }
  return $bestBooks;
}
?>
