<?php
function aws_signed_request($region, $params, $public_key, $private_key) {
    $method = "GET";
    $host = "ecs.amazonaws.".$region;
    $uri = "/onca/xml";
    $params["Service"] = "AWSECommerceService";
    $params["AWSAccessKeyId"] = $public_key;
    $params["Timestamp"] = gmdate("Y-m-d\TH:i:s\Z",time());
    $params["Version"] = "2009-03-31";
    ksort($params);
    $canonicalized_query = array();
    foreach ($params as $param=>$value)
    {
        $param = str_replace("%7E", "~", rawurlencode($param));
        $value = str_replace("%7E", "~", rawurlencode($value));
        $canonicalized_query[] = $param."=".$value;
    }
    $canonicalized_query = implode("&", $canonicalized_query);
    $string_to_sign = $method."\n".$host."\n".$uri."\n".$canonicalized_query;
    $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $private_key, True));
    $signature = rawurlencode($signature);
    $request = "http://".$host.$uri."?".$canonicalized_query."&Signature=".$signature;
    return $request;
}

function AmazonImage($keywords = "", $author = "") {
	$parameters = array(
	  'Operation'       => "ItemSearch",
	  'SearchIndex'     => "Books",
	  "Author"          => $author,
	  "Keywords"        => $keywords,
	  'ItemPage'        => 1,
	  'AssociateTag'    => "1234567890",
	  'ResponseGroup'   => "Medium"
	);
	
	/*
	$parameters=array(
	  'Operation'       =>'ItemLookup',
	  'ItemId'          =>'9787121092862',
	  "IdType"	        =>	"EAN"    ,
	  "SearchIndex"     => "Books",
	  'AssociateTag'    => '1234567890'
	);
	
	$parameters=array(
	  'Operation'       =>'ItemLookup',
	  'ItemId'          =>'7508263243',
	  "IdType"	        =>	"ASIN"    ,
	  'AssociateTag'    => '1234567890'
	);
	*/

	$Alocale=array('uk' => 'UK', 'us' => 'USA'); $Aserver=array('uk' => array('ext' => 'co.uk', 'nor' => 'http://www.amazon.co.uk', 'xml' => 'http://xml-eu.amazon.com'), 'us' => array('ext' => 'com', 'nor' => 'http://www.amazon.com', 'xml' => 'http://xml.amazon.com')); $Amode=array('us' => array('books' => 'books'), 'uk' => array('books-uk' => 'books')); $Asearchtype=array('ActorSearch'        => 'Actor/Actress'  ,'ArtistSearch'       => 'Artist/Musician','AsinSearch'         => 'ASIN/ISBN'      ,'AuthorSearch'       => 'Author'         ,'BlendedSearch'      => 'Blended'        ,'BrowseNodeSearch'   => 'Browse node'    ,'DirectorSearch'     => 'Director'       ,'ExchangeSearch'     => 'Exchange'       ,'KeywordSearch'      => 'Keyword'        ,'ListmaniaSearch'    => 'Listmania'      ,'ManufacturerSearch' => 'Manufacturer'   ,'MarketplaceSearch'  => 'Marketplace'    ,'PowerSearch'        => 'Power'          ,'SellerSearch'       => 'Seller'         ,'SimilaritySearch'   => 'Similarity'     ,'TextStreamSearch'   => 'TextStream'     ,'UpcSearch'          => 'UPC'            ,'WishlistSearch'     => 'Wishlist'); $Asearchtype_restaurant=array('American'  => 'American', 'English'   => 'English'); $Aerror=array('There are no exact matches for the search.'=>'print'); $Aoperation=array( 'BrowseNodeLookup'=>array( 'RequiredParameters'=>array( 'BrowseNodeId' ), 'OptionalParameters'=>array( ), 'ResponseGroups'=>array( 'BrowseNodeInfo','MostGifted','MostWishedFor','NewReleases','TopSellers', ), ), 'CartAdd'=>array( 'RequiredParameters'=>array(       'CartId',       'HMAC', 'Item', 'Items', 'ListItemId', 'Quantity', ),     'OptionalParameters'=>array(       'ASIN','MergeCart','OfferListingId',     ), 'ResponseGroups'=>array( 'Cart','CartSimilarities','CartTopSellers','CartNewReleases',     ),   ),   'CartClear'=>array(     'RequiredParameters'=>array( 'CartId', 'HMAC', ), 'OptionalParameters'=>array(       'MergeCart',     ),     'ResponseGroups'=>array( 'Cart', ),   ),   'CartCreate'=>array( 'RequiredParameters'=>array( 'AssociateTag','Item','Items','ListItemId','Quantity',     ),     'OptionalParameters'=>array( 'ASIN','MergeCart','OfferListingId', ), 'ResponseGroups'=>array(       'Cart','CartSimilarities','CartTopSellers','CartNewReleases',     ),   ),   'CartGet'=>array( 'RequiredParameters'=>array( 'CartId',       'HMAC', ),     'OptionalParameters'=>array(       'MergeCart',     ),     'ResponseGroups'=>array( 'Cart','CartSimilarities','CartTopSellers','CartNewReleases',     ),   ),   'CartModify'=>array(     'RequiredParameters'=>array( 'ASIN','CartId','HMAC','Item','ListItemId','OfferListingId','Quantity',     ),     'OptionalParameters'=>array(       'Items','MergeCart',     ),     'ResponseGroups'=>array(       'Cart','CartSimilarities','CartTopSellers','CartNewReleases',     ),   ),   'CustomerContentLookup'=>array(     'RequiredParameters'=>array(       'CustomerId',     ),     'OptionalParameters'=>array(       'ReviewPage','TagPage','TagSort','TagsPerPage',     ),     'ResponseGroups'=>array(       'CustomerInfo',       'CustomerReviews',       'CustomerLists',       'CustomerFull', 'TagsSummary',       'Tags',       'TaggedItems',       'TaggedListmaniaLists',       'TaggedGuides',     ),   ),   'CustomerContentSearch'=>array( 'RequiredParameters'=>array(     ),     'OptionalParameters'=>array(       'CustomerPage','Email','Name',     ),     'ResponseGroups'=>array( 'CustomerInfo',     ),   ),   'Help'=>array(     'RequiredParameters'=>array(     ),     'OptionalParameters'=>array( ), 'ResponseGroups'=>array(     ),   ),   'ItemLookup'=>array(     'RequiredParameters'=>array( 'ItemId',     ),     'OptionalParameters'=>array( 'Condition',       'DeliveryMethod',       'ISPUPostalCode',       'IdType',       'MerchantId', 'OfferPage', 'RelatedItemPage', 'RelationshipType',       'ReviewPage',       'ReviewSort',       'SearchIndex',       'TagPage',       'TagSort', 'TagsPerPage',       'VariationPage',     ),     'ResponseGroups'=>array(        'Small', 'Accessories', 'AlternateVersions', 'BrowseNodes',  'Collections', 'EditorialReview', 'Images', 'ItemAttributes', 'ItemIds', 'Large', 'ListmaniaLists', 'Medium', 'MerchantItemAttributes', 'OfferFull', 'OfferListings', 'Offers', 'OfferSummary', 'PromotionalTag',        'PromotionDetails', 'PromotionSummary', 'RelatedItems', 'Reviews',        'SalesRank', 'SearchInside', 'Similarities', 'Subjects', 'Tags',        'TagsSummary', 'Tracks', 'VariationImages', 'VariationMatrix', 'VariationMinimum', 'VariationOffers', 'Variations',        'VariationSummary',     ),   ),   'ItemSearch'=>array(     'RequiredParameters'=>array(       'SearchIndex'=>array(         'Actor',         'Artist',         'AudienceRating',         'Author',         'Brand',         'BrowseNode',         'City ',         'Composer',         'Conductor',         'Director',         'Keywords',         'Manufacturer',         'MusicLabel',         'Neighborhood',         'Orchestra',         'Power',         'Publisher',         'TextStream',         'Title',       ),     ),     'OptionalParameters'=>array(       'Actor', 'Artist', 'AudienceRating', 'Author', 'Availability', 'Brand', 'BrowseNode', 'City ', 'Composer', 'Condition', 'Conductor', 'Cuisine', 'DeliveryMethod', 'Director', 'DisableParentAsinSubstitution', 'ISPUPostalCode', 'Itempage', 'Keywords', 'Manufacturer', 'MaximumPrice', 'MerchantId', 'MinimumPrice',       'MusicLabel',       'Neighborhood',       'Orchestra',       'PostalCode',       'Power',       'Publisher',       'RelatedItemPage',       'RelationshipType',       'ReleaseDate',       'ReviewSort',       'Sort',       'State',       'TagPage',       'TagSort',       'TagsPerPage',       'TextStream',       'Title',     ),     'ResponseGroups'=>array(       'Small',       'Accessories',       'AlternateVersions',       'BrowseNodes',       'Collections',       'EditorialReview',       'Images',       'ItemAttributes',       'ItemIds',       'Large',       'ListmaniaLists',       'Medium',       'MerchantItemAttributes',       'OfferFull',       'OfferListings',       'Offers',       'OfferSummary',       'PromotionalTag',       'PromotionDetails',       'PromotionSummary',       'RelatedItems',       'Reviews',       'SalesRank',       'SearchBins',       'SearchInside',       'Similarities',       'Subjects',       'Tags',       'TagsSummary',       'Tracks',       'VariationMatrix',       'VariationMinimum',       'VariationOffers',       'Variations',       'VariationSummary',     ),   ),   'ListLookup'=>array(     'RequiredParameters'=>array(       'ListId','ListType',     ),     'OptionalParameters'=>array(       'Condition',       'DeliveryMethod',       'ISPUPostalCode',       'IsOmitPurchasedItems',       'MerchantId',       'ProductGroup',       'ProductPage',       'ReviewSort',       'Sort',     ),     'ResponseGroups'=>array(        'ListInfo', 'Accessories', 'BrowseNodes', 'EditorialReview',        'Images', 'ItemAttributes', 'ItemIds', 'Large', 'ListFull', 'ListItems',        'ListmaniaLists', 'Medium', 'OfferFull', 'OfferListings', 'Offers',        'OfferSummary', 'PromotionDetails', 'PromotionSummary',        'Reviews', 'SalesRank', 'Similarities', 'Small', 'Subjects', 'Tracks',        'VariationMinimum', 'Variations', 'VariationSummary',     ),   ),   'ListSearch'=>array(     'RequiredParameters'=>array(       'ListType',     ),     'OptionalParameters'=>array(       'City',       'Email',       'FirstName',       'LastName',       'ListPage',       'Name',       'State',     ),     'ResponseGroups'=>array(       'ListInfo','ListMinimum',     ),   ),   'SellerListingLookup'=>array(     'RequiredParameters'=>array(       'Id','IdType',     ),     'OptionalParameters'=>array(       'SellerId',     ),     'ResponseGroups'=>array(       'SellerListing',     ),   ),   'SellerListingSearch'=>array(     'RequiredParameters'=>array(       'SellerId',     ),     'OptionalParameters'=>array(       'Keywords','ListingPage','OfferStatus','Sort',     ),     'ResponseGroups'=>array(       'SellerListing',     ),   ),   'SellerLookup'=>array(     'RequiredParameters'=>array(       'SellerId', ), 'OptionalParameters'=>array( 'FeedbackPage', ), 'ResponseGroups'=>array( 'Seller', ), ), 'SimilarityLookup'=>array( 'RequiredParameters'=>array( 'ItemId', ), 'OptionalParameters'=>array(       'Condition',       'DeliveryMethod',       'ISPUPostalCode',       'MerchantId',       'ReviewSort',       'SimilarityType',     ),     'ResponseGroups'=>array(       'Small (D)', 'Accessories', 'BrowseNodes', 'EditorialReview',       'Images', 'ItemAttributes', 'ItemIds', 'Large', 'ListmaniaLists',       'Medium', 'OfferFull', 'OfferListings', 'Offers', 'OfferSummary',       'PromotionDetails', 'PromotionSummary', 'Reviews',       'SalesRank', 'Similarities', 'Subjects', 'Tracks',       'VariationMinimum', 'Variations', 'VariationSummary',     ),   ),   'TagLookup'=>array(     'RequiredParameters'=>array(       'TagName',     ),     'OptionalParameters'=>array(       'Count','CustomerId','TagPage','TagSort',     ),     'ResponseGroups'=>array('TaggedItems (D)', 'TagsSummary (D)', 'Accessories', 'AlternateVersions', 'BrowseNodes', 'Collections', 'EditorialReview', 'Images', 'ItemAttributes', 'ItemIds', 'Large', 'ListmaniaLists', 'Medium', 'MerchantItemAttributes', 'OfferFull', 'OfferListings', 'Offers','OfferSummary', 'PromotionalTag', 'PromotionDetails', 'PromotionSummary',                   'Reviews', 'SalesRank', 'SearchInside', 'Similarities', 'Small',                   'Subjects', 'TaggedGuides', 'TaggedListmaniaLists', 'Tags',                   'Tracks',     ),   ),   'TransactionLookup'=>array(     'RequiredParameters'=>array(       'TransactionId',     ),     'OptionalParameters'=>array(     ),     'ResponseGroups'=>array(       'PartnerTransactionDetails','TransactionDetails',     ),   ),   'VehiclePartLookup'=>array(     'RequiredParameters'=>array(       'ItemId',     ),     'OptionalParameters'=>array(       'BedId',       'BodyStyleId',       'BrakesId',       'DriveTypeId',       'EngineId',       'FitmentCount',       'FitmentPage',       'IdType',       'MakeId',       'MfrBodyCodeId',       'ModelId',       'SpringTypesId',       'SteeringId',       'TransmissionId',       'TrimId',       'WheelbaseId',       'Year',     ),     'ResponseGroups'=>array(       'HasPartCompatibility',       'Fitments',       'VehiclePartFit',     ),   ),   'VehiclePartSearch'=>array(     'RequiredParameters'=>array(       'MakeId',       'ModelId',       'Year',     ),     'OptionalParameters'=>array(       'BedId',       'BodyStyleId',       'BrakesId',       'Brand',       'BrowseNodeId',       'Count','DriveTypeId','EngineId','FrontItemId','MfrBodyCodeId','PartPageDirection','SpringTypesId','SteeringId','TransmissionId','TrimId',),'ResponseGroups'=>array('VehicleParts','HasPartCompatibility','PartBrandBinsSummary','PartBrowseNodeSummary','VehiclePartFit',),),'VehicleSearch'=>array('RequiredParameters'=>array(),'OptionalParameters'=>array('MakeId','ModelId','TrimId','Year',),'ResponseGroups'=>array('VehicleYears','VehicleMakes','VehicleModels','VehicleOptions','VehicleTrims',),),); $Asearchindex=array('All','Books');
	
	$ext = $Aserver['us']['ext'];
	$file_data = $ext;
	ksort($parameters);
	foreach ($parameters as $i=>$d) {
	  $file_data.='&'.$i.'='.$d;
	}
	$file = aws_signed_request($ext,$parameters,$public_key = "AKIAIXI6VIU4Q5PDCUNA", $private_key = "NTjiAWszW5eFOj5GYKgak+7GhkczsFoZRDR7J7Lb");
	$filecontents = file_get_contents($file);
	$A = simplexml_load_string($filecontents);
	
	$items = $A->Items;
	$aitems = (array)$items;
	$items_sub = $aitems['Item'];
	
	if (is_array($items_sub) and $items_sub[0]) {
	  foreach ($items_sub as $i => $E) {
	    $d = $E->MediumImage;
	    $iu = $d->URL;
	    return "<img src='$iu' border='0'></a><br />\n";
	    break;
	  }
	}
}
?>