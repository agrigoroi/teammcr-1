<?php

/*
 * Posts for and returns XML from cam.ac.uk based on given URL.
 * Always uses cambrdgedb database.
 */

class CamXMLQuery {
    
    public $result = null;
    
    public function getXML($searchArg) {
        $this->result = null;
        $this->result = file_get_contents("http://www.lib.cam.ac.uk/api/voyager/newtonSearch.cgi?searchArg=".$searchArg."&databases=cambrdgedb");

        // Not using this anymore
        //$result = file_get_contents("http://www.lib.cam.ac.uk/api/voyager/newtonSearch.cgi?searchArg=".$searchArg."&databases=cambrdgedb");
        //http_get("http://www.lib.cam.ac.uk/api/voyager/newtonSearch.cgi", array("searchArg"=>$searchArg, "databases"=>"cambrdgedb"), $result);
    }
}

?>
