<?php

/*
 * CamXMLBookReader - Class encapsulates functionality for processing book XML
 *                      returned from lib.cam.ac.uk Newton search app into array form.
 * 
 * Results returned in CamXMLBookRecord.
 */

class CamXMLBookReader {
    
    public $result = null;
    
    public function processXML($XML) {
        $this->result = null;
        $xmlReader = new XMLReader();
        //$xmlAccessor = new SimpleXML.

        $xmlReader->xml($XML);
        $doc = new DOMDocument;
                
        // Open search_results
        while($xmlReader->read() && $xmlReader->name !== "bib_record") {}
        // Loop through bib_records
        while($xmlReader->name === "bib_record"){
            $booknodes = simplexml_import_dom($doc->importNode($xmlReader->expand(), true));
            
            // Save book into new record and append to master array
            $newRec = new CamXMLBookRecord();
            
            // TODO, seperate book details and populate record members
            $title = (string)($booknodes->title);
            $ISBN = (string)($booknodes->isbn);
            $series = (string)($booknodes->series);
            $ISSN = (string)($booknodes->issn);
            $author = (string)($booknodes->author);
            
            $newRec->setData($ISBN,$title,$author,$series,$ISSN);
            
            if(!$this->result){$this->result = Array($newRec);}
            else {array_push($this->result, $newRec);}
            
            $xmlReader->next("bib_record");
        }
        
    }   
}

?>
