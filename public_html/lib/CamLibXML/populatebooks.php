<?php

/*
 * TeamMCR - DevXS
 * Andy Slack
 * 
 * Compiles set number of books using given set of subjects, then populates
 * table 'book' with results. Aimimg to get as many books as possible.
 */

set_time_limit(0);

require_once("CamXMLBookRecord.php");
require_once("CamXMLBookReader.php");
require_once("CamXMLQuery.php");

// Create objects & vars
$camQuery = new CamXMLQuery();
$camReader = new CamXMLBookReader();
$booksFound = 0;
$searchAreas = null;
$bookRecords[] = null;

// Populate search areas
$searchAreas =  array("computing", "physics", "chemistry", "vetenary", "mathematics", "engineering", "english", "french", "geography", "art", "history", "german", "politics","economics","biology");

// Loop through search areas until we have enough books.
foreach($searchAreas as $searchTerm) {
    // Grab XML if successful
    $camQuery->getXML($searchTerm);
    if($camQuery->result) $currentXML = $camQuery->result;
    // Process XML into records & append to found books
    $camReader->processXML($currentXML);
    if($camReader->result) {
        // Loop through records and append each to master array
        foreach($camReader->result as $record) {
            if(!$bookRecords){$bookRecords = array($record);}
            else {array_push($bookRecords, $record);}
        }
    }
}

//print(count($bookRecords)." books found.");

// Parse results into SQL statement
$sql = "INSERT INTO books (ISBN, title, author, series, ISSN) VALUES ";

foreach($bookRecords as $bookRec){
$bookRec = $bookRecords[4];
    if($bookRec->title){
        $sql = $sql."('','".trim($bookRec->ISBN,"'`")."','".trim($bookRec->title,"'`")."','".trim($bookRec->author,"'`")."','".trim($bookRec->series,"'`")."','".trim($bookRec->ISSN,"`'")."'),";
    }
    
}
$sql = rtrim($sql,",");
$sql = $sql.";";

file_put_contents("sql_books_insert.sql",$sql);

?>
