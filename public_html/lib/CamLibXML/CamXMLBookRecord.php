<?php

/*
 * Simple record for ease of addressing columns later.
 */

class CamXMLBookRecord {
    public $ISBN = null;
    public $title = null;
    public $author = null;
    public $series = null;
    public $ISSN = null;
    
    public function setData($a_isbn, $a_title, $a_author, $a_series, $a_issn){
        $this->ISBN = $a_isbn;
        $this->title = $a_title;
        $this->author = $a_author;
        $this->series = $a_series;
        $this->ISSN = $a_issn;
    }
}

?>
