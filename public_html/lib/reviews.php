<?php
/* DevXS - Team MCR
 */

/* getReviews($book) - Returns all reviews for a specific book in an array.
 * $book - The ID of the book.
 *
 * Array Format -
 *		Key 		-> Value
 *		ratingid 	-> The ID of the rating.
 * 		username	-> The username of the student who posted the review.
 *		uniid 		-> The university of the student at the time of the review.
 *		courseid 	-> The course of the student at the time of the review.
 *		rating 		-> The rating represented as a 1 or a -1, used to signal a thumbs up or thumbs down.
 *		review 		-> The content of the users' review.
 */

function getReviewsByBook($book) {  
	$query = mysql_query("SELECT students.username, students.name, rating.ratingid, rating.uniid, rating.courseid, rating.rating, rating.review, universities.* FROM rating, students LEFT JOIN universities ON students.universityid=universities.ucas_code WHERE rating.bookid = '".$book."' ORDER BY rating.timestamp DESC");
	$reviews = array();
	
	while($review = mysql_fetch_array($query, MYSQL_ASSOC)) {
		array_push($reviews, $review);
	}
	
	return $reviews;
}

/* getReviewsByUser($user) - Returns all reviews by a specific user in an array.
 * $user - The id of the user.
 *
 * Array Format -
 *		Key 		-> Value
 *		-------------------------
 *		ratingid 	-> The ID of the rating.
 * 		bookid		-> The ID of the book being reviewed.
 *		uniid 		-> The university of the student at the time of the review.
 *		courseid 	-> The course of the student at the time of the review.
 *		rating 		-> The rating represented as a 1 or a -1, used to signal a thumbs up or thumbs down.
 *		review 		-> The content of the users' review.
 */

function getReviewsByUser($user) {
	$query = mysql_query("SELECT rating.ratingid, rating.bookid, rating.uniid, rating.courseid, rating.rating, rating.review FROM rating, books LEFT JOIN students s ON s WHERE rating.bookid = books.bookid AND rating.studentid = ".$user." ORDER BY rating.timestamp DESC");
	$reviews = array();
	
	while($review = mysql_fetch_array($query, MYSQL_ASSOC)) {
		array_push($reviews, $review);
	}
	
	return $reviews;
}
?>