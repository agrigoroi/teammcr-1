<?php
session_start();

/************************************
* TeamMCR  @ #DEVXS		 *
* -- Farkie (11:10 / 12/11/2011) *
*											 *	
*************************************
* index.php							 *
************************************/

$list_of_uni = split("\n",file_get_contents("unis.txt"));

foreach($list_of_uni as $uni) {
	$list  .= '<option value="'.trim($uni).'">'.trim($uni).'</option>'.PHP_EOL;
	}

	if($_POST['login'] && $_POST['_token'] == $_SESSION['token']) {
		if($_POST['username']  && $_POST['password'])  {
		
		$_SESSION['university'] = $_POST['uni']; // Insert validation here...
		$_SESSION['username'] = $_POST['username'];
			header("Location: /userarea.php");
			exit();
			} else {
				$feedback = 'error';
				$message[] = 'Your user/password information is incorrect';
			}
	}

require_once($_SERVER['DOCUMENT_ROOT'].'/includes/header.php');
$token = $_SESSION['token'] = md5(uniqid(microtime()));

drawFeedback($feedback,$message);
?>
	<form action="" method="post">
	<input type="hidden" name="_token" value="<?=$token?>">
		<h2>Welcome to Library@DevXS</h2>
			<h4>Please login below</h4>
		
		
		<fieldset>
			<label for="uni">University:</label>
				<select name="uni" id="uni">
					<?=$list?>
				</select>
		</fieldset>
		<fieldset>
			<label for="username">Username: </label>
				<input type="text" name="username" id="username">
		</fieldset>
		<fieldset>
			<label for="password">Password: </label>
				<input type="password" name="password" id="password">
		</fieldset>
		<fieldset>
			<input type="submit" name="login" value="Login" id="login">
		</fieldset>
	</form>
<?
require_once($_SERVER['DOCUMENT_ROOT'].'/includes/footer.php');
?>