<?php
include("./lib/config.php");
include("./lib/database.php");
include("./lib/session.php");

if(!$session->logged_in) {
  header("Location: ./login.php");
}

$req_user = $session->username;
$req_user_info = $database->getUserInfo($req_user);

function getUniversity($university_id) {
	$result = mysql_query("SELECT * FROM `universities` WHERE `ucas_code` = '" . $university_id . "'");
	$row = mysql_fetch_row($result);
	
	return $row[1];
}

include("./templates/header.php");
?>
<h2>Welcome to Library@DevXS</h2>
Your university is: <?php print(getUniversity($req_user_info['universityid'])); ?><br />
Your username is: <?php print($req_user_info['username']); ?><br />
Your name is: <?php print($req_user_info['name']); ?><br />
Your email is: <?php print($req_user_info['email']); ?><br />
<a href="./logout.php">Click here to logout</a>
<?php
include("./templates/footer.php");
?>