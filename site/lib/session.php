<?php
class Session {
  var $username;
  var $userid;
  var $time;
  var $logged_in;
  var $userinfo = array();
  
  function Session() {
    $this->time = time();
    $this->startSession();
  }
  
  function startSession() {
    global $database;
    
    session_start();
    $this->logged_in = $this->checkLogin();
    
    if(!$this->logged_in) {
      $this->username = $_SESSION['username'] = "Guest";
    }else{
      mysql_query("UPDATE users SET lastactive = '".$this->time."' WHERE username = '".$this->username."'");
    }
  }
  
  function checkLogin() {
    global $database;
    
    if(isset($_COOKIE['user']) && isset($_COOKIE['uid'])) {
      $this->username = $_SESSION['username'] = $_COOKIE['user'];
      $this->userid   = $_SESSION['userid']   = $_COOKIE['uid'];
    }
    
    if(isset($_SESSION['username']) && isset($_SESSION['userid']) && $_SESSION['username'] != "Guest") {
      if($database->confirmUserID($_SESSION['username'], $_SESSION['userid']) != 0) {
        unset($_SESSION['username']);
        unset($_SESSION['userid']);
        
        return false;
      }
      
      $this->userinfo = $database->getUserInfo($_SESSION['username']);
      $this->username = $this->userinfo['username'];
      $this->userid   = $this->userinfo['userid'];
      
      return true;
    }else{
      return false;
    }
  }
  
  function generateRandID() {
    return md5($this->generateRandStr(16));
  }
  
  function generateRandStr($length) {
    $randstr = "";
    
    for($i=0; $i < $length; $i++) {
      $randnum = mt_rand(0, 61);
      if($randnum < 10) {
        $randstr .= chr($randnum + 48);
      }else if($randnum < 36) {
        $randstr .= chr($randnum+55);
      }else{
        $randstr .= chr($randnum+61);
      }
    }
    
    return $randstr;
  }
};

$session = new Session;
?>